require "rails_helper"

RSpec.describe VoteCommentsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/vote_comments").to route_to("vote_comments#index")
    end

    it "routes to #new" do
      expect(:get => "/vote_comments/new").to route_to("vote_comments#new")
    end

    it "routes to #show" do
      expect(:get => "/vote_comments/1").to route_to("vote_comments#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/vote_comments/1/edit").to route_to("vote_comments#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/vote_comments").to route_to("vote_comments#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/vote_comments/1").to route_to("vote_comments#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/vote_comments/1").to route_to("vote_comments#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/vote_comments/1").to route_to("vote_comments#destroy", :id => "1")
    end

  end
end
